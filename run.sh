xhost +local:root

docker run -it -d --rm -e DISPLAY=$DISPLAY \
        -v /tmp/.X11-unix:/tmp/.X11-unix:ro \
        --name="ros_dev" \
        osrf/ros:kinetic-desktop-full \

docker cp init.sh ros_dev:/
#docker run osrf/ros:kinetic-desktop-full ./init.sh
