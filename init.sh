source /opt/ros/kinetic/setup.bash

mkdir -p ~/catkin-ws/src

cd ~/catkin-ws/src

git clone https://github.com/ros-drivers/driver_common.git
git clone https://github.com/ros-simulation/gazebo_ros_pkgs.git
git clone https://github.com/ros/common_msgs.git

cd ..

catkin_make

cd src
git clone https://github.com/tu-darmstadt-ros-pkg/hector_gazebo.git
cd ..
catkin_make
source ~/catkin-ws/devel/setup.bash

mkdir ~/gazebo
cd ~/gazebo
mkdir models
mkdir worlds

export GAZEBO_MODEL_PATH=/root/gazebo/models/
export GAZEBO_RESOURCE_PATH=/root/gazebo/

cd ~
git clone https://git.uwaterloo.ca/krandhawa/fydp-ros.git
cd fydp-ros

cp -R models ~/gazebo/
cp -R worlds ~/gazebo/

mkdir build
cd build
cmake ..

source /opt/ros/kinetic/setup.bash
QT_X11_NO_MITSHM=1

roslaunch ../vehicle_world.launch -v
